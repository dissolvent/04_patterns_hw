import usersDatabase from '../db/users';
import roomsDatabase from '../db/rooms';

export function userLogin(id, username, socket) {
  if (usersDatabase.getUserByUsername(username)) {
    socket.emit('USER_EXISTS');
  } else {
    const user = usersDatabase.addUser(id, username);
    socket.emit('UPDATE_ROOMS', roomsDatabase.getActiveRooms());
    return user;
  }
}

export function userDisconnected(id) {
  usersDatabase.deleteUser(id);
}

export function getUserByUsename(username) {
  return usersDatabase.getUserByUsername(username);
}

