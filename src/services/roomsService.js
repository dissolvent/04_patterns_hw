import roomsDatabase from '../db/rooms';
import { MAXIMUM_USERS_FOR_ONE_ROOM } from '../config';

export const createNewRoom = (roomId, socket) => {
  if (roomsDatabase.getRoomById(roomId)) {
    socket.emit('ROOM_EXISTS');
    return;
  }
  return roomsDatabase.createRoom(roomId, socket.id);
};

const onLeaveEmit = (roomId, socket) => {
  socket.leave(roomId, () => {
    socket.emit('LEAVE_ROOM_DONE');
  });
};

export const leaveRoom = (roomId, socket) => {
  roomsDatabase.removeUserFromRoom(roomId, socket.id);
  const room = roomsDatabase.getRoomById(roomId);
  if (!room?.users.length) {
    roomsDatabase.deleteRoom(roomId);
    onLeaveEmit(roomId, socket);
    return;
  }

  roomsDatabase.setRoomFull(roomId, false);
  onLeaveEmit(roomId, socket);
  socket.to(roomId).emit('UPDATE_ROOM', room);
  return room;
};

export const joinRoom = (roomId, socket) => {
  if (roomsDatabase.getRoomFull(roomId)) {
    return false;
  }
  const room = roomsDatabase.addUserToRoom(roomId, socket.id);
  if (room?.users?.length === MAXIMUM_USERS_FOR_ONE_ROOM) {
    roomsDatabase.setRoomFull(roomId, true);
  }
  socket.join(roomId, () => {
    socket.emit('JOIN_ROOM_DONE', room);
    socket.to(roomId).emit('UPDATE_ROOM', room);
  });
  return room;
};
