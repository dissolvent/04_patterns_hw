import { random } from 'lodash';
import { SECONDS_TIMER_BEFORE_START_GAME, COMMENTATOR_NOTIFY_CHARACTERS_LEFT_TO_FINISH } from '../config';

export const getGreetingQuote = () => {
  // eslint-disable-next-line
  const greeting = 'На улице сейчас немного пасмурно, но на Львов Арена сейчас просто замечательная атмосфера: двигатели рычат, зрители улыбаются а гонщики едва заметно нервничают и готовят своих железных коней к заезду. А комментировать всё это действо буду я, Эскейп Энтерович и я рад вас приветствовать со словами Доброго Вам дня, господа!';
  return greeting;
};

export const getUsernameWithTransport = username => {
  const transport = ['HyperX', 'Logitech', 'Razer', 'SteelSeries', 'A4Tech'];
  return `${username} на ${transport[random(0, (transport.length - 1))]}`;
};

export const getParticipantsQuote = users => {
  const participants = 'Сегодня в гонке учавствуют:\n';
  const usersOnTransport = users.map((user => getUsernameWithTransport(user.username)));
  return usersOnTransport.reduce((accumulator, value) => `${accumulator} ${value}.\n`, participants);
};

export const sortUsersByProgress = users => users
  .concat()
  .sort((a, b) => b.progress.currentCharacter - a.progress.currentCharacter);

export const getSortedByProgressUsernames = users => {
  const sorted = sortUsersByProgress(users);
  return sorted.map(user => user.username);
};

export const secondsSpentForRace = (userFinished, gameStarted) => Math.round(
  ((userFinished - gameStarted) / 1000) - SECONDS_TIMER_BEFORE_START_GAME
);

export const mapQuotesToUsernames = (quotes, usernames) => usernames.map(
  (username, index) => `${quotes[index]} ${username}`
);

export const getUsersProgressQuote = users => {
  const topThreeUsers = getSortedByProgressUsernames(users).slice(0, 3);
  const quotes = ['В гонке лидирует', ', за ним', '. Замыкает тройку лидеров'];
  const usersWithQuotes = mapQuotesToUsernames(quotes, topThreeUsers);
  return usersWithQuotes.join('');
};

export const isUserNearFinish = (currentCharacter, totalCharacters) => (
  (totalCharacters - currentCharacter) === COMMENTATOR_NOTIFY_CHARACTERS_LEFT_TO_FINISH
);
